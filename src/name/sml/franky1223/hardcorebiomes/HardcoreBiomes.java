package name.sml.franky1223.hardcorebiomes;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
 
public final class HardcoreBiomes extends JavaPlugin {
	
	public static void main(String[] args) {
		System.out.println("You must put this file in the plugins folder, you dipshit.");
	}

	private String VERSION = "0.7.2";
    BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
	
	private long ticks;
	private long frequency;
	private boolean enabled;
	private boolean verbose;
	private boolean biomeAct;
	private boolean altitudeAct;
	private boolean weatherAct;
	private boolean armorAct;
	private boolean itemAct;
	private boolean otherAct;
	private float stabilization;
	private float modThresholdMin;
	private float modThresholdMax;
	private float tempThresholdMin;
	private float tempThresholdMax;
	private float biomeMin;
	private float biomeMax;
	private float altitudeMin;
	private float altitudeMax;
	private float weatherMin;
	private float weatherMax;
	private float armorMin;
	private float armorMax;
	private float itemMin;
	private float itemMax;
	private float otherMin;
	private float otherMax;
	private HashMap<String, Float> temperature = new HashMap<String, Float>();
	private HashMap<String, Float> modifier = new HashMap<String, Float>();
	private HashMap<String, Integer> stabilizer = new HashMap<String, Integer>();
	private String textPrefix = ChatColor.GOLD.toString();
	private String textInfoPrefix = ChatColor.YELLOW.toString();
	private String textErrorPrefix = ChatColor.RED.toString();
	private String[][] textPluginInfo = {{
		textPrefix+"        HARDCORE BIOMES v"+VERSION,
		textPrefix+"This plugin is brought to you by",
		textPrefix+"  - franky1223 (François Allard) &",
		textPrefix+"  - youmy (Tommy Teasdale)",
		textPrefix+"/X are commands, (X) are optional subcommands, ",
		textPrefix+"[X] are optional parameters, <X> are required parameters.",
		textPrefix+"Almost all subcommands are also aliases, so /habi temp (info) [player] can become /temp [player]",
		textInfoPrefix+"            [Page 1/2]",
	},{
		textPrefix+"/habi: Show this message.",
		textPrefix+"/habi temp (info) [player]: See your temperature (or someone's).",
		textPrefix+"/habi temp loc [player]: See what your (or someone's) surroundings affect you (or someone).",
		textPrefix+"/habi temp set [player] <number>: Set your temperature (or someone's) to another value.",
		textPrefix+"/habi temp heat [player] <number>: Add degrees to your (or someone's) temperature.",
		textPrefix+"/habi temp cool [player] <number>: Remove degrees to your (or someone's) temperature.",
		textPrefix+"/habi reload: Reload Hardcore Biomes config file.",
		textInfoPrefix+"            [Page 2/2]",
	}};
	private String textNotEnabled = textErrorPrefix+"The plugin is not enabled.";
	private String textNoPerm = textErrorPrefix+"You do not have sufficient rights.";
	private String textNotPlayer = textErrorPrefix+"There is no such player online.";
	private String textMustBePlayer = textErrorPrefix+"This command can only be run by a player.";
	private String textNotEnoughArgs = textErrorPrefix+"Not enough arguments.";
	private String textTooManyArgs = textErrorPrefix+"Too many arguments";
	private String textInvalidFormat = textErrorPrefix+"Invalid number format.";
	
	@SuppressWarnings("unchecked")
	@Override
    public void onEnable() {
        ticks = 0;
        this.saveDefaultConfig();
        loadConfig();
    	if (enabled) {
    		try {
				temperature = (HashMap<String, Float>) FileHelper.load(this.getDataFolder() + File.separator + "temperature.bin");
    		} catch (EOFException e) {
    			temperature = new HashMap<String, Float>();
			} catch (Exception e) {
				e.printStackTrace();
			}
    		try {
    			modifier = (HashMap<String, Float>) FileHelper.load(this.getDataFolder() + File.separator + "modifier.bin");
    		} catch (EOFException e) {
    			modifier = new HashMap<String, Float>();
			} catch (Exception e) {
				e.printStackTrace();
			}
    		try {
    			stabilizer = (HashMap<String, Integer>) FileHelper.load(this.getDataFolder() + File.separator + "stabilizer.bin");
    		} catch (EOFException e) {
    			stabilizer = new HashMap<String, Integer>();
			} catch (Exception e) {
				e.printStackTrace();
			}
    		pluginLoop();
	    	getLogger().info("Hardcore Biomes v"+VERSION+" enabled.");
    	} else {
	    	getLogger().info("Hardcore Biomes is disabled in the config.");
    	}
    }
	
    @Override
    public void onDisable() {
    	if (enabled) {
			try {
				FileHelper.save(temperature, this.getDataFolder() + File.separator + "temperature.bin");
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				FileHelper.save(modifier, this.getDataFolder() + File.separator + "modifier.bin");
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				FileHelper.save(stabilizer, this.getDataFolder() + File.separator + "stabilizer.bin");
			} catch (Exception e) {
				e.printStackTrace();
			}
    		getLogger().info("Hardcore Biomes disabled.");
    	}
    }

    public void pluginLoop() {
        scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
			@Override
			public void run() {
				if (verbose) getLogger().info("Plugin ticks: "+ticks);
				ticks++;
				for (Player player : Bukkit.getOnlinePlayers()) {
					World world = player.getWorld();
					Biome biome = world.getBiome(player.getLocation().getBlockX(), player.getLocation().getBlockZ());
	    			float environmentTemp = 0;
    				if (verbose) getLogger().info("Player: "+player.getName());
	    			if (biomeAct) {
		    			float biomeTemp = getTempFromBiome(biome);
	    				environmentTemp += biomeTemp;
	    				if (verbose) getLogger().info("Biome: "+biomeTemp);
	    			}
	    			if (altitudeAct) {
	    				float altitudeTemp = getTempFromAltitude(player.getLocation().getBlockY());
	    				environmentTemp += altitudeTemp;
	    				if (verbose) getLogger().info("Altitude: "+altitudeTemp);
	    			}
	    			if (weatherAct) {
	        			float weatherTemp = getTempFromWeather(player);
	        			environmentTemp += weatherTemp;
	    				if (verbose) getLogger().info("Weather: "+weatherTemp);
	    			}
	    			if (armorAct) {
	        			float armorTemp = getTempFromArmor(player);
	    				environmentTemp += armorTemp;
	    				if (verbose) getLogger().info("Armor: "+armorTemp);
	    			}
	    			if (otherAct) {
	        			float otherTemp = getTempFromOther(player);
	    				environmentTemp += otherTemp;
	    				if (verbose) getLogger().info("Other: "+otherTemp);
	    			}
	    			environmentTemp = MathHelper.clamp_float(environmentTemp, -1.0F, 1.0F);
    				modifier.put(player.getName(), environmentTemp);
    				if (verbose) getLogger().info("Environment effects: "+getCurrentModifier(player.getName()));
    				if (stabilizer.containsKey(player) && stabilizer.get(player) > 0) {
    					stabilizer.put(player.getName(), stabilizer.get(player) - 1);
    					environmentTemp *= stabilization;
	    				if (verbose) getLogger().info("Temperature stabilized! Environment effects are now reduced to "+(stabilization*100)+"%");
    				}
    				if (getCurrentModifier(player.getName()) < modThresholdMin || getCurrentModifier(player.getName()) > modThresholdMax) {
	    				temperature.put(player.getName(), getCurrentTemperature(player.getName()) + (environmentTemp * 0.05F));
	    				if (verbose) getLogger().info("Is affecting temperature?: TRUE");
    				} else {
	    				temperature.put(player.getName(), getCurrentTemperature(player.getName()) * 0.95F);
	    				if (verbose) getLogger().info("Is affecting temperature?: FALSE");
    				}
    				if (verbose) getLogger().info("Temperature: "+getCurrentTemperature(player.getName()));
    				if (getCurrentTemperature(player.getName()) < tempThresholdMin || getCurrentTemperature(player.getName()) > tempThresholdMax) {
    					if (ticks % 20 == 0) player.damage(Math.abs(getCurrentTemperature(player.getName()) * 0.5));
	    				if (verbose) getLogger().info("Is hurting player?: TRUE");
    				} else {
	    				if (verbose) getLogger().info("Is hurting player?: FALSE");
    				}
    				if (verbose) getLogger().info("Health: "+player.getHealth());
				}
			}
        }, 0, frequency*20);
    }

	private void loadConfig() {
        enabled = this.getConfig().getBoolean("hardcorebiomes.enabled", true);
        if (enabled) {
	        verbose = this.getConfig().getBoolean("hardcorebiomes.verbose", false);
	        frequency = this.getConfig().getLong("hardcorebiomes.frequency", 2);
	        stabilization = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.biome.min", 0.25);
	        modThresholdMin = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.environment.min", -0.15);
	        modThresholdMax = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.environment.max", 0.15);
	        tempThresholdMin = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.temperature.min", -1.5);
	        tempThresholdMax = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.temperature.min", 1.5);
	    	biomeAct = this.getConfig().getBoolean("hardcorebiomes.modifiers.biome.enabled", true);
	    	biomeMin = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.biome.min", -0.7);
	    	biomeMax = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.biome.max", 0.7);
	    	altitudeAct = this.getConfig().getBoolean("hardcorebiomes.modifiers.altitude.enabled", true);
	    	altitudeMin = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.altitude.min", -0.2);
	    	altitudeMax = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.altitude.max", 0.0);
	    	weatherAct = this.getConfig().getBoolean("hardcorebiomes.modifiers.weather.enabled", true);
	    	weatherMin = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.weather.min", -0.2);
	    	weatherMax = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.weather.max", 0.0);
	    	armorAct = this.getConfig().getBoolean("hardcorebiomes.modifiers.armor.enabled", true);
	    	armorMin = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.armor.min", -0.2);
	    	armorMax = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.armor.max", 0.5);
	    	itemAct = this.getConfig().getBoolean("hardcorebiomes.modifiers.item.enabled", true);
	    	itemMin = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.item.min", -0.2);
	    	itemMax = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.item.max", 0.2);
	    	otherAct = this.getConfig().getBoolean("hardcorebiomes.modifiers.other.enabled", true);
	    	otherMin = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.other.min", -0.3);
	    	otherMax = (float)this.getConfig().getDouble("hardcorebiomes.modifiers.other.max", 0.3);
        }
	}
	
	@SuppressWarnings("unchecked")
	@Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    	if (cmd.getName().equalsIgnoreCase("hardcorebiomes") || cmd.getName().equalsIgnoreCase("habi") || cmd.getName().equalsIgnoreCase("hb")) {
    		if (args.length > 0 && !args[0].isEmpty() && args[0].length() > 0) {
        		if (args[0].equalsIgnoreCase("temp") || args[0].equalsIgnoreCase("temperature")) {
        			if (enabled) {
		        		if (args.length > 1 && !args[1].isEmpty() && args[1].length() > 0) {
		        			if (args[1].equalsIgnoreCase("info") || args[1].equalsIgnoreCase("informations")) {
		        				return getTemperature((Player) sender, args, 2);
		        			} else if (args[1].equalsIgnoreCase("loc") || args[1].equalsIgnoreCase("location")) {
		        				return getBiomeTemperature((Player) sender, args, 2);
		        			} else if (args[1].equalsIgnoreCase("set") || args[1].equalsIgnoreCase("modify")) {
		        				return setTemperature((Player) sender, args, 2);
		    				} else if (args[1].equalsIgnoreCase("heat") || args[1].equalsIgnoreCase("add")) {
		    					return heatPlayer((Player) sender, args, 2);
		    				} else if (args[1].equalsIgnoreCase("cool") || args[1].equalsIgnoreCase("remove")) {
		    					return coolPlayer((Player) sender, args, 2);
			    			} else {
		        				return getTemperature((Player) sender, args, 1);
		    				}
		    			} else {
		    				return getTemperature((Player) sender, args, 1);
		    			}
        			} else {
        				sender.sendMessage(textNotEnabled);
        				return true;
        			}
        		} else if (args[0].equalsIgnoreCase("reload") || args[0].equalsIgnoreCase("restart")) {
        			if (sender.hasPermission("hardcorebiomes.reload")) {
            			if (sender instanceof Player) sender.sendMessage(textInfoPrefix+"Reloading config...");
            			getLogger().info("Reloading config...");
            			try {
							FileHelper.save(temperature, this.getDataFolder() + File.separator + "temperature.bin");
						} catch (Exception e) {
							e.printStackTrace();
						}
            			try {
							FileHelper.save(modifier, this.getDataFolder() + File.separator + "modifier.bin");
						} catch (Exception e) {
							e.printStackTrace();
						}
            			try {
							FileHelper.save(stabilizer, this.getDataFolder() + File.separator + "stabilizer.bin");
						} catch (Exception e) {
							e.printStackTrace();
						}
            			scheduler.cancelAllTasks();
        				reloadConfig();
        				loadConfig();
        				pluginLoop();
        	    		try {
        					temperature = (HashMap<String, Float>) FileHelper.load(this.getDataFolder() + File.separator + "temperature.bin");
        	    		} catch (EOFException e) {
        	    			temperature = new HashMap<String, Float>();
        				} catch (Exception e) {
        					e.printStackTrace();
        				}
        	    		try {
        	    			modifier = (HashMap<String, Float>) FileHelper.load(this.getDataFolder() + File.separator + "modifier.bin");
        	    		} catch (EOFException e) {
        	    			modifier = new HashMap<String, Float>();
        				} catch (Exception e) {
        					e.printStackTrace();
        				}
        	    		try {
        	    			stabilizer = (HashMap<String, Integer>) FileHelper.load(this.getDataFolder() + File.separator + "stabilizer.bin");
        	    		} catch (EOFException e) {
        	    			stabilizer = new HashMap<String, Integer>();
        				} catch (Exception e) {
        					e.printStackTrace();
        				}
        				if (sender instanceof Player) sender.sendMessage(textInfoPrefix+"Config reloaded.");
            			getLogger().info("Config reloaded.");
            			return true;
        			} else {
            			sender.sendMessage(textNoPerm);
            			return true;
        			}
        		} else if (args[0].matches("\\d*")) {
        			if (sender.hasPermission("hardcorebiomes.info")) {
        				for (String s : textPluginInfo[MathHelper.clamp_int(Integer.parseInt(args[0]), 1, 2)]) {
        					sender.sendMessage(s);
        				}
        	    		return true;
        			} else {
            			sender.sendMessage(textNoPerm);
            			return true;
        			}
        		}
    		}
			if (sender.hasPermission("hardcorebiomes.info")) {
				for (String s : textPluginInfo[0]) {
					sender.sendMessage(s);
				}
	    		return true;
			} else {
    			sender.sendMessage(textNoPerm);
    			return true;
			}
    	} else if (cmd.getName().equalsIgnoreCase("temp") || cmd.getName().equalsIgnoreCase("temperature")) {
    		if (enabled) {
	    		if (args.length > 0 && !args[0].isEmpty() && args[0].length() > 0) {
	    			if (args[0].equalsIgnoreCase("info") || args[0].equalsIgnoreCase("informations")) {
	    				return getTemperature((Player) sender, args, 1);
	    			} else if (args[0].equalsIgnoreCase("loc") || args[0].equalsIgnoreCase("location")) {
	    				return getBiomeTemperature((Player) sender, args, 1);
	    			} else if (args[0].equalsIgnoreCase("set") || args[0].equalsIgnoreCase("modify")) {
	    				return setTemperature((Player) sender, args, 1);
					} else if (args[0].equalsIgnoreCase("heat") || args[0].equalsIgnoreCase("add")) {
						return heatPlayer((Player) sender, args, 1);
					} else if (args[0].equalsIgnoreCase("cool") || args[0].equalsIgnoreCase("remove")) {
						return coolPlayer((Player) sender, args, 1);
	    			} else {
	    				return getTemperature((Player) sender, args, 0);
					}
				} else {
					return getTemperature((Player) sender, args, 0);
				}
    		} else {
    			sender.sendMessage(textNotEnabled);
    			return true;
    		}
    	} else if (cmd.getName().equalsIgnoreCase("loc")) {
    		if (enabled) {
    			return getBiomeTemperature((Player) sender, args, 0);
    		} else {
    			sender.sendMessage(textNotEnabled);
    			return true;
    		}
    	} else if (cmd.getName().equalsIgnoreCase("heat")) {
    		if (enabled) {
    			return heatPlayer((Player) sender, args, 0);
    		} else {
    			sender.sendMessage(textNotEnabled);
    			return true;
    		}
    	} else if (cmd.getName().equalsIgnoreCase("cool")) {
    		if (enabled) {
    			return coolPlayer((Player) sender, args, 0);
    		} else {
    			sender.sendMessage(textNotEnabled);
    			return true;
    		}
    	} else {
    		return false;
    	}
    }

	private boolean getTemperature(Player sender, String[] args, int i) {
		if (sender.hasPermission("hardcorebiomes.temp.info")) {
			if (args.length > i && !args[i].isEmpty() && args[i].length() > 0) {
	    		if (sender.hasPermission("hardcorebiomes.temp.info.others")) {
    				if (Bukkit.getPlayer(args[i]) != null) {
	    				sender.sendMessage(Bukkit.getPlayer(args[i]).getName()+"'s temperature is "+getCurrentTemperature(Bukkit.getPlayer(args[i]).getName())+" degrees with a modification factor of "+getCurrentModifier(Bukkit.getPlayer(args[i]).getName()));
	    				return true;
    				} else {
	        			sender.sendMessage(textNotPlayer);
	        			return true;
    				}
	    		} else {
	    			sender.sendMessage(textNoPerm);
		    		return true;
	    		}
			} else {
	        	if (sender instanceof Player) {
	        		sender.sendMessage(textInfoPrefix+"Your temperature is: "+getCurrentTemperature(((Player) sender).getName())+" degrees with a modification factor of "+getCurrentModifier(((Player) sender).getName()));
	        		return true;
	    		} else {
	    			sender.sendMessage(textMustBePlayer);
	    			return true;
				}
			}
		} else {
			sender.sendMessage(textNoPerm);
			return true;
		}
	}

	private boolean getBiomeTemperature(Player sender, String[] args, int i) {
		if (sender.hasPermission("hardcorebiomes.temp.loc")) {
			if (args.length > i && !args[i].isEmpty() && args[i].length() > 0) {
	    		if (sender.hasPermission("hardcorebiomes.temp.loc.others")) {
	    			Player player = Bukkit.getPlayer(args[i]);
    				if (player != null) {
						Biome biome = player.getWorld().getBiome(player.getLocation().getBlockX(), player.getLocation().getBlockZ());
	    				sender.sendMessage(textInfoPrefix+player.getName()+"'s location is in biome "+biome.name());
	    				sender.sendMessage("Is is currently "+getTempFromBiome(biome)+"° there,");
	    				sender.sendMessage("with altitude effect of "+getTempFromAltitude(player.getLocation().getBlockY())+"°.");
	    				if (player.getWorld().hasStorm()) sender.sendMessage("It is currently raining in this world.");
	    				sender.sendMessage("The time is now "+getAnalogTime(player)+" and it adds "+getTempFromWeather(player)+"°.");
	    				return true;
    				} else {
	        			sender.sendMessage(textNotPlayer);
	        			return true;
    				}
	    		} else {
	    			sender.sendMessage(textNoPerm);
		    		return true;
	    		}
			} else {
				if (sender instanceof Player) {
					Biome biome = sender.getWorld().getBiome(sender.getLocation().getBlockX(), sender.getLocation().getBlockZ());
					sender.sendMessage(textInfoPrefix+"Your location is in biome "+biome.name());
    				sender.sendMessage(textInfoPrefix+sender.getName()+"'s location is in biome "+biome.name());
    				sender.sendMessage("Is is currently "+getTempFromBiome(biome)+"° there,");
    				sender.sendMessage("with altitude effect of "+getTempFromAltitude(sender.getLocation().getBlockY())+"°.");
    				if (sender.getWorld().hasStorm()) sender.sendMessage("It is currently raining in this world.");
    				sender.sendMessage("The time is now "+getAnalogTime(sender)+" and it adds "+getTempFromWeather(sender)+"°.");
					return true;
				} else {
					sender.sendMessage(textMustBePlayer);
					return true;
				}
			}
		} else {
			sender.sendMessage(textNoPerm);
			return true;
		}
	}
	
	private boolean setTemperature(Player sender, String[] args, int i) {
		if (sender.hasPermission("hardcorebiomes.temp.set")) {
			if (args.length > i && !args[i].isEmpty() && args[1].length() > 0) {
				if (args[i].matches("^([+-](?=\\.?\\d))?(\\d{1,2})?(\\.\\d+)?$")) {
					if (sender instanceof Player) {
						if (args.length <= i+1 || args[i+1].isEmpty() || args[i+1].length() <= 0) {
							float value = MathHelper.clamp_float(Float.parseFloat(args[i]), -10.0F, 10.0F);
							temperature.put(sender.getName(), value);
		    				sender.sendMessage(textInfoPrefix+"Your temperature has been set to: "+value);
							return true;
						} else {
							sender.sendMessage(textTooManyArgs);
							return true;
						}
					} else {
						sender.sendMessage(textMustBePlayer);
						return true;
					}
				} else if (args.length > i+1 && !args[i+1].isEmpty() && args[i+1].length() > 0) {
	    			if (sender.hasPermission("hardcorebiomes.temp.set.others")) {
	    				if (Bukkit.getPlayer(args[i]) != null) {
    	    				if (args[i+1].matches("^([+-](?=\\.?\\d))?(\\d{1,2})?(\\.\\d+)?$")) {
    							float value = MathHelper.clamp_float(Float.parseFloat(args[i+1]), -10.0F, 10.0F);
    							temperature.put(Bukkit.getPlayer(args[i]).getName(), value);
    	    					sender.sendMessage(textInfoPrefix+Bukkit.getPlayer(args[i]).getName()+"'s temperature has been set to: "+value);
								Bukkit.getPlayer(args[i]).sendMessage(textInfoPrefix+"Your temperature has been set to: "+value);
    	    					return true;
    	    				} else {
    	    					sender.sendMessage(textInvalidFormat);
    	    					return true;
    	    				}
        				} else {
    	        			sender.sendMessage(textNotPlayer);
    	        			return true;
        				}
	    			} else {
	    				sender.sendMessage(textNoPerm);
		    			return true;
	    			}
    			} else {
					sender.sendMessage(textInvalidFormat);
					return true;
    			}
			} else {
				sender.sendMessage(textNotEnoughArgs);
				return true;
			}
		} else {
			sender.sendMessage(textNoPerm);
			return true;
		}
	}

	private boolean heatPlayer(Player sender, String[] args, int i) {
		if (sender.hasPermission("hardcorebiomes.temp.heat")) {
			if (args.length > i && !args[i].isEmpty() && args[i].length() > 0) {
				if (args[i].matches("^([+-](?=\\.?\\d))?(\\d{1,2})?(\\.\\d+)?$")) {
					if (sender instanceof Player) {
						if (args.length <= i+1 || args[i+1].isEmpty() || args[i+1].length() <= 0) {
							float value = MathHelper.clamp_float(getCurrentTemperature(sender.getName()) + Float.parseFloat(args[i]), -10.0F, 10.0F);
							temperature.put(sender.getName(), value);
							sender.sendMessage(textInfoPrefix+value+" has been added to your temperature.");
							return true;
						} else {
							sender.sendMessage(textTooManyArgs);
							return true;
						}
					} else {
						sender.sendMessage(textMustBePlayer);
						return true;
					}
				} else if (args.length > i+1 && !args[i+1].isEmpty() && args[i+1].length() > 0) {
					if (sender.hasPermission("hardcorebiomes.temp.heat.others")) {
						if (Bukkit.getPlayer(args[i]) != null) {
							if (args[i+1].matches("^([+-](?=\\.?\\d))?(\\d{1,2})?(\\.\\d+)?$")) {
								float value = MathHelper.clamp_float(getCurrentTemperature(sender.getName()) + Float.parseFloat(args[i+1]), -10.0F, 10.0F);
    							temperature.put(Bukkit.getPlayer(args[i]).getName(), value);
								sender.sendMessage(textInfoPrefix+value+" has been added to "+Bukkit.getPlayer(args[i]).getName()+"'s temperature.");
								Bukkit.getPlayer(args[i]).sendMessage(textInfoPrefix+value+" has been added to your temperature.");
								return true;
							} else {
								sender.sendMessage(textInvalidFormat);
								return true;
							}
						} else {
							sender.sendMessage(textNotPlayer);
							return true;
						}
					} else {
	    				sender.sendMessage(textNoPerm);
		    			return true;
					}
				} else {
					sender.sendMessage(textInvalidFormat);
					return true;
				}
			} else {
				sender.sendMessage(textNotEnoughArgs);
				return true;
			}
		} else {
			sender.sendMessage(textNoPerm);
			return true;
		}
	}

	private boolean coolPlayer(Player sender, String[] args, int i) {
		if (sender.hasPermission("hardcorebiomes.temp.cool")) {
			if (args.length > i && !args[i].isEmpty() && args[i].length() > 0) {
				if (args[i].matches("^([+-](?=\\.?\\d))?(\\d{1,2})?(\\.\\d+)?$")) {
					if (sender instanceof Player) {
					if (args.length <= i+1 || args[i+1].isEmpty() || args[i+1].length() <= 0) {
						float value = MathHelper.clamp_float(getCurrentTemperature(sender.getName()) - Float.parseFloat(args[i]), -10.0F, 10.0F);
						temperature.put(sender.getName(), value);
						sender.sendMessage(textInfoPrefix+value+" has been removed from your temperature.");
						return true;
					} else {
						sender.sendMessage(textTooManyArgs);
						return true;
					}
					} else {
						sender.sendMessage(textMustBePlayer);
						return true;
					}
				} else if (args.length > i+1 && !args[i+1].isEmpty() && args[i+1].length() > 0) {
	    			if (sender.hasPermission("hardcorebiomes.temp.cool.others")) {
	    				if (Bukkit.getPlayer(args[i]) != null) {
    	    				if (args[i+1].matches("^([+-](?=\\.?\\d))?(\\d{1,2})?(\\.\\d+)?$")) {
    							float value = MathHelper.clamp_float(getCurrentTemperature(sender.getName()) - Float.parseFloat(args[i+1]), -10.0F, 10.0F);
    							temperature.put(Bukkit.getPlayer(args[i]).getName(), value);
    	    					sender.sendMessage(textInfoPrefix+value+" has been removed from "+Bukkit.getPlayer(args[i]).getName()+"'s temperature.");
								Bukkit.getPlayer(args[i]).sendMessage(textInfoPrefix+value+" has been removed from your temperature.");
    	    					return true;
    	    				} else {
    	    					sender.sendMessage(textInvalidFormat);
    	    					return true;
    	    				}
        				} else {
    	        			sender.sendMessage(textNotPlayer);
    	        			return true;
        				}
	    			} else {
	    				sender.sendMessage(textNoPerm);
		    			return true;
	    			}
    			} else {
					sender.sendMessage(textInvalidFormat);
					return true;
    			}
			} else {
				sender.sendMessage(textNotEnoughArgs);
				return true;
			}
		} else {
			sender.sendMessage(textNoPerm);
			return true;
		}
	}

	private String getAnalogTime(Player player) {
		int hours = (int) (player.getWorld().getTime() / 1000);
		float minutes = (int) (player.getWorld().getTime() / 10) - (hours * 100);
		return hours+":"+minutes;
	}
	
	private float getTempFromBiome(Biome biome) {
		float temp = 0.0F;
		switch (biome) {
			case FROZEN_RIVER:
			case ICE_PLAINS:
			case ICE_PLAINS_SPIKES:
			case ICE_MOUNTAINS:
			case FROZEN_OCEAN:
				temp = 1.0F * biomeMin;
				break;
			case COLD_BEACH:
			case COLD_TAIGA:
			case COLD_TAIGA_HILLS:
			case COLD_TAIGA_MOUNTAINS:
				temp = 0.8F * biomeMin;
				break;
			case EXTREME_HILLS:
			case EXTREME_HILLS_MOUNTAINS:
			case TAIGA:
			case TAIGA_HILLS:
			case TAIGA_MOUNTAINS:
			case SKY:
			case MEGA_TAIGA:
			case MEGA_TAIGA_HILLS:
			case MEGA_SPRUCE_TAIGA:
			case MEGA_SPRUCE_TAIGA_HILLS:
			case EXTREME_HILLS_PLUS:
			case EXTREME_HILLS_PLUS_MOUNTAINS:
			case STONE_BEACH:
				temp = 0.4F * biomeMin;
				break;
			case OCEAN:
			case DEEP_OCEAN:
			case SMALL_MOUNTAINS:
			case PLAINS:
			case SUNFLOWER_PLAINS:
			case FOREST:
			case FOREST_HILLS:
			case FLOWER_FOREST:
			case RIVER:
			case MUSHROOM_ISLAND:
			case MUSHROOM_SHORE:
			case SWAMPLAND:
			case SWAMPLAND_MOUNTAINS:
			case BEACH:
			case BIRCH_FOREST:
			case BIRCH_FOREST_MOUNTAINS:
			case BIRCH_FOREST_HILLS:
			case BIRCH_FOREST_HILLS_MOUNTAINS:
			case ROOFED_FOREST:
			case ROOFED_FOREST_MOUNTAINS:
				temp = 0.0F;
				break;
			case JUNGLE:
			case JUNGLE_MOUNTAINS:
			case JUNGLE_EDGE:
			case JUNGLE_EDGE_MOUNTAINS:
			case JUNGLE_HILLS:
				temp = 0.4F * biomeMax;
				break;
			case DESERT:
			case DESERT_HILLS:
			case DESERT_MOUNTAINS:
			case SAVANNA:
			case SAVANNA_MOUNTAINS:
			case SAVANNA_PLATEAU:
			case SAVANNA_PLATEAU_MOUNTAINS:
			case MESA:
			case MESA_BRYCE:
			case MESA_PLATEAU:
			case MESA_PLATEAU_MOUNTAINS:
			case MESA_PLATEAU_FOREST:
			case MESA_PLATEAU_FOREST_MOUNTAINS:
				temp = 0.8F * biomeMax;
				break;
			case HELL:
				temp = 1.0F * biomeMax;
				break;
			default: 
				temp = 0.0F;
				break;
		}
		return MathHelper.clamp_float(temp, biomeMin, biomeMax);
	}

	private float getTempFromAltitude(float height) {
		if (height > 80) {
			return MathHelper.clamp_float((float)Math.sin((height - 80) / (100 / altitudeMin)), altitudeMin, altitudeMax);
		} else if (height < 50) {
			return MathHelper.clamp_float((float)Math.sin(-(height - 50) / (100 / altitudeMin)), altitudeMin, altitudeMax);
		} else {
			return 0;
		}
	}

	private float getTempFromWeather(Player player) {
		float time = (float) (((player.getWorld().getFullTime()) % 24000.0F) / 12000.0F * Math.PI);
		time = (float)Math.sin(time); // Ranges between -1 and 1
		float dist = weatherMax + weatherMin;
		time = time * (dist / 2);
		float weather = player.getWorld().hasStorm() ? -dist / 2 : 0;
		for (int i =(int) player.getLocation().getBlockY(); i < player.getWorld().getMaxHeight(); i++) {
			if (!player.getWorld().getBlockAt(player.getLocation().getBlockX(), i, player.getLocation().getBlockZ()).isEmpty()) {
				return MathHelper.clamp_float((time + weather) * 0.5F, weatherMin, weatherMax);
			}
		}
		return MathHelper.clamp_float(time + weather, weatherMin, weatherMax);
	}
	
	private float getTempFromArmor(Player player) {
		float armorValue = 0.0F;
		ItemStack helmet = player.getEquipment().getHelmet();
		ItemStack chestplate = player.getEquipment().getChestplate();
		ItemStack leggings = player.getEquipment().getLeggings();
		ItemStack boots = player.getEquipment().getBoots();
		HashMap<Material, Float> helmetValues = new HashMap<Material, Float>();
			helmetValues.put(Material.LEATHER_HELMET, 0.2F);
			helmetValues.put(Material.IRON_HELMET, -0.2F);
			helmetValues.put(Material.CHAINMAIL_HELMET, -0.1F);
			helmetValues.put(Material.GOLD_HELMET, 0.1F);
			helmetValues.put(Material.DIAMOND_HELMET, 0.0F);
		HashMap<Material, Float> chestplateValues = new HashMap<Material, Float>();
			chestplateValues.put(Material.LEATHER_CHESTPLATE, 0.3F);
			chestplateValues.put(Material.IRON_CHESTPLATE, -0.3F);
			chestplateValues.put(Material.CHAINMAIL_CHESTPLATE, -0.15F);
			chestplateValues.put(Material.GOLD_CHESTPLATE, 0.15F);
			chestplateValues.put(Material.DIAMOND_CHESTPLATE, 0.0F);
		HashMap<Material, Float> leggingsValues = new HashMap<Material, Float>();
			leggingsValues.put(Material.LEATHER_LEGGINGS, 0.2F);
			leggingsValues.put(Material.IRON_LEGGINGS, -0.2F);
			leggingsValues.put(Material.CHAINMAIL_LEGGINGS, -0.1F);
			leggingsValues.put(Material.GOLD_LEGGINGS, 0.1F);
			leggingsValues.put(Material.DIAMOND_LEGGINGS, 0.0F);
		HashMap<Material, Float> bootsValues = new HashMap<Material, Float>();
			bootsValues.put(Material.LEATHER_BOOTS, 0.1F);
			bootsValues.put(Material.IRON_BOOTS, -0.1F);
			bootsValues.put(Material.CHAINMAIL_BOOTS, -0.05F);
			bootsValues.put(Material.GOLD_BOOTS, 0.05F);
			bootsValues.put(Material.DIAMOND_BOOTS, 0.0F);
		if (helmet != null && helmetValues.containsKey(helmet.getType()))
			armorValue += helmetValues.get(helmet.getType());
		if (chestplate != null && chestplateValues.containsKey(chestplate.getType()))
			armorValue += chestplateValues.get(chestplate.getType());
		if (leggings != null && leggingsValues.containsKey(leggings.getType()))
			armorValue += leggingsValues.get(leggings.getType());
		if (boots != null && bootsValues.containsKey(boots.getType()))
			armorValue += bootsValues.get(boots.getType());
		if (helmet != null && chestplate != null && leggings != null && boots != null) {
			if (helmet.getType() == Material.LEATHER_HELMET && chestplate.getType() == Material.LEATHER_CHESTPLATE && 
				leggings.getType() == Material.LEATHER_LEGGINGS && boots.getType() == Material.LEATHER_BOOTS) {
				armorValue = 1.0F;
			}
			if (helmet.getType() == Material.IRON_HELMET && chestplate.getType() == Material.IRON_CHESTPLATE && 
				leggings.getType() == Material.IRON_LEGGINGS && boots.getType() == Material.IRON_BOOTS) {
				armorValue = -1.0F;
			}
			if (helmet.getType() == Material.CHAINMAIL_HELMET && chestplate.getType() == Material.CHAINMAIL_CHESTPLATE && 
				leggings.getType() == Material.CHAINMAIL_LEGGINGS && boots.getType() == Material.CHAINMAIL_BOOTS) {
				armorValue = -0.5F;
			}
			if (helmet.getType() == Material.GOLD_HELMET && chestplate.getType() == Material.GOLD_CHESTPLATE && 
				leggings.getType() == Material.GOLD_LEGGINGS && boots.getType() == Material.GOLD_BOOTS) {
				armorValue = 0.5F;
			}
			if (helmet.getType() == Material.DIAMOND_HELMET && chestplate.getType() == Material.DIAMOND_CHESTPLATE && 
				leggings.getType() == Material.DIAMOND_LEGGINGS && boots.getType() == Material.DIAMOND_BOOTS) {
				armorValue = 0.0F;
			}
		}
		if (armorValue > 0) armorValue *= armorMax;
		else armorValue *= armorMin;
		return MathHelper.clamp_float(armorValue, armorMin, armorMax);
	}
	
	@EventHandler
	private void onItemEat(PlayerItemConsumeEvent event) {
		if (itemAct) {
			ItemStack item = event.getItem();
			Player player = event.getPlayer();
			if ((item.getType() == Material.CAKE) || item.getType() == Material.MUSHROOM_SOUP) {
				temperature.put(player.getName(), temperature.get(player.getName()) + itemMax);
			} else if ((item.getType() == Material.POTION) || item.getType() == Material.MILK_BUCKET) {
				temperature.put(player.getName(), temperature.get(player.getName()) + itemMin);
			}
			stabilizer.put(player.getName(), 30);
		}
	}

	private float getTempFromOther(Player player) {
		List<Entity> entities = player.getNearbyEntities(3.0, 3.0, 3.0);
		float mult = 0.0F;
		if (entities != null) {
			while (entities.iterator().hasNext()) {
				Entity entity = entities.iterator().next();
				if (entity instanceof Blaze) mult += 0.3F;
				if (entity instanceof Chicken) mult += 0.1F;
				if (entity instanceof Cow) mult += 0.2F;
				if (entity instanceof EnderDragon) mult -= 0.54F;
				if (entity instanceof Enderman) mult -= 0.1F;
				if (entity instanceof Fireball) mult += 0.2F;
				if (entity instanceof Ghast) mult -= 0.2F;
				if (entity instanceof Horse) mult += 0.2F;
				if (entity instanceof LargeFireball) mult += 0.3F;
				if (entity instanceof LightningStrike) mult += 0.4F;
				if (entity instanceof MagmaCube) mult += 0.2F;
				if (entity instanceof MushroomCow) mult += 0.2F;
				if (entity instanceof Pig) mult += 0.1F;
				if (entity instanceof Sheep) mult += 0.2F;
				if (entity instanceof SmallFireball) mult += 0.1F;
				if (entity instanceof Snowman) mult -= 0.2F;
				if (entity instanceof Wither) mult -= 0.6F;
			}
		}
		return MathHelper.clamp_float(mult, otherMin, otherMax);
	}

    private float getCurrentTemperature(String player) {
		return temperature.containsKey(player) ? MathHelper.clamp_float(temperature.get(player), -10.0F, 10.0F) : 0.0F;
	}

    private float getCurrentModifier(String player) {
		return modifier.containsKey(player) ? MathHelper.clamp_float(modifier.get(player), -1.0F, 1.0F) : 0.0F;
	}
    
}

final class FileHelper {

	public static void save(HashMap<String, ?> obj, String path) throws Exception {
		File f = new File(path);
		f.createNewFile();
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f));
		oos.writeInt(obj.size());
		for (Entry<String, ?> entry : obj.entrySet()) {
			oos.writeUTF(entry.getKey());
			oos.writeObject(entry.getValue());
		}
		oos.flush();
		oos.close();
	}

	public static HashMap<String, ?> load(String path) throws Exception {
		File f = new File(path);
		f.createNewFile();
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
		HashMap<String, Object> result = new HashMap<String, Object>();
		int size = ois.readInt();
		for (int i = 0; i < size; i++) {
			result.put(ois.readUTF(), ois.readObject());
		}
		ois.close();
		return result;
	}
	
}

final class MathHelper {

    public static float clamp_float(float val, float min, float max) {
        return Math.max(min, Math.min(max, val));
    }

	public static int clamp_int(int val, int min, int max) {
        return Math.max(min, Math.min(max, val));
	}
    
}